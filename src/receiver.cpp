#include "receiver.h"


// ------------------------------------------
//                   PPM reading related pins
// ------------------------------------------
int interruptPin = WKP;
volatile unsigned long *rawValues = NULL;
volatile unsigned long *validValues = NULL;
volatile unsigned long *completeValues = NULL;
int pulseCounter = 0;
unsigned long microsAtLastPulse = 0;
bool dataAvailable = false;
// ------------------------------------------


// ------------------------------------------
bool receiverAvailable(){
  if (dataAvailable){
    dataAvailable = false;
    return true;
  }
  return false;
}

// ------------------------------------------

void printReceiver(){
  if (t.talk(INFO)){
    for (int i = 0; i < channelAmount; i++){
      Serial.print(completeValues[i]); Serial.print("\t");
    }
    Serial.println();
  }
}

// ------------------------------------------

void handleInterrupt() {
    // Remember the current micros() and calculate the time since the last pulseReceived()
    unsigned long previousMicros = microsAtLastPulse;
    microsAtLastPulse = micros();
    unsigned long time = microsAtLastPulse - previousMicros;

    if (time > 2100) {
        /* If the time between pulses was long enough to be considered an end
         * of a signal frame, prepare to read channel values from the next pulses */
        pulseCounter = 0;
        dataAvailable = false;
    }
    else {
        // Store times between pulses as channel values
        if (pulseCounter < channelAmount) {
            rawValues[pulseCounter] = time;
            if (time >= 1000 - 10 && time <= 2000 + 10) {
                validValues[pulseCounter] = constrain(time, 1000, 2000);
            }
        }
        ++pulseCounter;
        if (pulseCounter == channelAmount){
          for (int i = 0; i < channelAmount; i++)
            completeValues[i] = validValues[i];
          dataAvailable = true;
        }
        else if (pulseCounter > channelAmount){
          if (t.talk(WARN))
            Serial.println("Too many pulse counts.");
        }
    }
}
