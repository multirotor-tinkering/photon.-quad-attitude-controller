/*
 * Project QuadControllerMark1
 * Description: The first iteration of the quadcopter controller. Attitude controller using two IMUs.
 * Author: Savva Morozov
 * Date: July 26 2020
 */
// #include <PPMReader.h>
#include <MPU6050.h>
#include "Particle.h"
#include "receiver.h"
#include "talker.h"


void setup() {
  // printing initializations
  Serial.begin(9600);
  t = Talker(INFO_TALK);


  // PPM and interrupt related variables
  {
    rawValues = new unsigned long[channelAmount];
    validValues = new unsigned long[channelAmount];
    completeValues = new unsigned long[channelAmount];
    pinMode(interruptPin, INPUT);
    attachInterrupt(interruptPin, handleInterrupt, RISING);
  }

}

void loop() {
  if (receiverAvailable()){
    printReceiver();
  }
  delay(200);
}
