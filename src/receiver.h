#pragma once

#include "talker.h"

// ------------------------------------------
//                   PPM reading related pins
// ------------------------------------------
extern int interruptPin; // all these must be externed because .ino includes receiver.h, not receier.cpp
static int channelAmount = 8;
extern volatile unsigned long *rawValues;
extern volatile unsigned long *validValues;
extern volatile unsigned long *completeValues;
extern int pulseCounter;
extern unsigned long microsAtLastPulse;
extern bool dataAvailable;
// ------------------------------------------
extern Talker t; // must be externed cause i use it in receiver.cpp
// ------------------------------------------

/**
 * @brief      return true if full ppm data packet is available
 *
 * @return     true if available
 */
bool receiverAvailable();

/**
 * @brief      print out the ppm packet
 *
 * @return     none
 */
void printReceiver();

/**
 * @brief      process the PPM pin interrupt. if full PPM packet is available - toggle dataAvailable variable
 *
 * @return     none
 */
void handleInterrupt();
