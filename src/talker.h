#pragma once

const int INFO_TALK =1;
const int STAT_TALK =2;
const int WARN_TALK =3;
const int NO_TALK =4;

const int INFO =1;
const int STAT =2;
const int WARN =3;

#include "Particle.h"

class Talker {
  public:
    int talk_type_ = 0;

  public:
    Talker(int type);

    /**
     * @brief      if message is in accordance to talking type, print the message header and return true.
     *
     * @param[in]  msg_type    Type of the message to be processed - INFO, STAT, WARN
     *
     * @return     true if data is to be printed
     */
    bool talk(int msg_type);
};
