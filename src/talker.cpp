#include "talker.h"

Talker t = NULL;

Talker::Talker(int type){
  talk_type_ = type;
}

bool Talker::talk(int msg_type){
  if (msg_type >= talk_type_){

    if (msg_type == INFO)
      Serial.print("INFO: ");
    else if (msg_type == STAT)
        Serial.print("STAT: ");
    else if (msg_type == WARN)
      Serial.print("WARN: ");
  }
  return (msg_type >= talk_type_);
}
